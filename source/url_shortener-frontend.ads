with AWS.Response;
with AWS.Status;

package URL_Shortener.Frontend is

   function Create_Alias (Request : in AWS.Status.Data) return AWS.Response.Data;

   function Get_Alias (Request : in AWS.Status.Data) return AWS.Response.Data;

end URL_Shortener.Frontend;
