with Aliases;
with URLs;

package URL_Shortener.Database is

   function Alias_Exist (Alias : Aliases.Alias) return Boolean;

   function URL_Exist (URL : URLs.URL) return Boolean;

   function Get_Alias (URL : URLs.URL) return Aliases.Alias;

   procedure New_Alias (URL : URLs.URL; Alias : Aliases.Alias);

   function Get_URL (Alias : Aliases.Alias) return URLs.URL;

   procedure Touch (Alias : Aliases.Alias);

end URL_Shortener.Database;
