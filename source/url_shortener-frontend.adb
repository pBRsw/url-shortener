with AWS.Messages;
with AWS.Parameters;

with Aliases;
with Hashes;
with URLs;
with FS_Config;

with URL_Shortener.Database;

package body URL_Shortener.Frontend is

   function Create_Alias (Request : in AWS.Status.Data) return AWS.Response.Data is
      Parameters : constant AWS.Parameters.List := AWS.Status.Parameters (Request);

      URL   : URLs.URL;
      Hash  : Hashes.Hash;
      Alias : Aliases.Alias;
   begin -- Create_Alias
      URL := To_Unbounded_String (AWS.Parameters.Get (Parameters, Name => "Long_URL") );

      if URLs.Has_Valid_Format (URL) and
         URLs.Is_Working (URL)
      then
         if URL_Shortener.Database.URL_Exist (URL) then
            Alias := URL_Shortener.Database.Get_Alias (URL);
         else
            Hash := Hashes.Generate (To_String (URL) );

            Alias := Aliases.Find_Shortest_Alias (Hash        => Hashes.To_String (Hash),
                                                  Min_Size    => 3,
                                                  Hash_Exists => Database.Alias_Exist'Access);

            Database.New_Alias (URL, Alias);
         end if;

         return AWS.Response.Acknowledge
            (Status_Code  => AWS.Messages.S200,
             Message_Body => "http://" & FS_Config.Get ("/config/text_hosting/fqdn") & '/' & Aliases.To_String (Alias) );
      else
         return AWS.Response.Acknowledge
            (Status_Code  => AWS.Messages.S500,
             Message_Body => "Borked");
      end if;
   end Create_Alias;

   function Get_Alias (Request : in AWS.Status.Data) return AWS.Response.Data is
      URL   : URLs.URL;
      Alias : Aliases.Alias;

      use type Ada.Strings.Unbounded.Unbounded_String;
   begin -- Get_Alias
      Alias := URLs.Alias (To_Unbounded_String (AWS.Status.URI (Request) ) );
      URL   := Database.Get_URL (Alias);

      Database.Touch (Alias);

      return AWS.Response.URL (To_String (URL) );
   end Get_Alias;

end URL_Shortener.Frontend;
