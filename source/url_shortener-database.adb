with FS_Config;

with PGAda.Connections;
with PGAda.Database;
with PGAda.Results;

package body URL_Shortener.Database is

   function Alias_Exist (Alias : Aliases.Alias) return Boolean is
      Result : PGAda.Results.Result;
   begin
      Result := PGAda.Database.Exec_Prepared
                  (Connection     => Conn,
                   Statement_Name => "findURL",
                   Parameters     => To_Unbounded_String (Aliases.To_String (Alias) ) );

      return PGAda.Results.Number_Of_Tuples (Result) > 0;
   end Alias_Exist;

   function URL_Exist (URL : URLs.URL) return Boolean is
      Result : PGAda.Results.Result;
      Status : PGAda.Results.Exec_Status;

      use type PGAda.Results.Exec_Status;
   begin
      Result := PGAda.Database.Exec_Prepared (Connection     => Conn,
                                              Statement_Name => "findAlias",
                                              Parameters     => URL);

      Status := PGAda.Results.Status (Result);

      return Status = PGAda.Results.Tuples_OK and
             PGAda.Results.Number_Of_Tuples (Result) > 0;
   end URL_Exist;

   function Get_Alias (URL : URLs.URL) return Aliases.Alias is
      Result : PGAda.Results.Result;
      Status : PGAda.Results.Exec_Status;

      Alias : Aliases.Alias;

      use type PGAda.Results.Exec_Status;
   begin
      Result := PGAda.Database.Exec_Prepared (Connection     => Conn,
                                              Statement_Name => "findAlias",
                                              Parameters     => URL);

      Status := PGAda.Results.Status (Result);

      if Status = PGAda.Results.Tuples_OK and
         PGAda.Results.Number_Of_Tuples (Result) > 0
      then
         Alias := Aliases.To_Alias (PGAda.Database.Get_Value (Result, 1, "alias") );
      else
         null; -- What to do?
      end if;

      return Alias;
   end Get_Alias;

   procedure New_Alias (URL : URLs.URL; Alias : Aliases.Alias) is
      Result : PGAda.Results.Result;
      Status : PGAda.Results.Exec_Status;

      use type PGAda.Results.Exec_Status;
   begin
      Result := PGAda.Database.Exec_Prepared
                  (Connection     => Conn,
                   Statement_Name => "insertAlias",
                   Parameters     => PGAda.Database.Parameter_List'(To_Unbounded_String (Aliases.To_String (Alias) ),
                                                                    URL) );
      Status := PGAda.Results.Status (Result);

      if Status /= PGAda.Results.Command_OK then
         raise Constraint_Error; -- What to do?
      end if;
   end New_Alias;

   function Get_URL (Alias : Aliases.Alias) return URLs.URL is
      Result : PGAda.Results.Result;
      Status : PGAda.Results.Exec_Status;

      URL : URLs.URL;

      use type PGAda.Results.Exec_Status;
   begin
      Result := PGAda.Database.Exec_Prepared
                  (Connection     => Conn,
                   Statement_Name => "findURL",
                   Parameters     => To_Unbounded_String (Aliases.To_String (Alias) ) );

      Status := PGAda.Results.Status (Result);

      if Status = PGAda.Results.Tuples_OK and
         PGAda.Results.Number_Of_Tuples (Result) > 0
      then
         URL := To_Unbounded_String (PGAda.Database.Get_Value (Result => Result, Tuple_Index => 1, Field_Name => "url") );
      else
         raise Constraint_Error;
      end if;

      return URL;
   end Get_URL;

   procedure Touch (Alias : Aliases.Alias) is
      Result : PGAda.Results.Result;
      Status : PGAda.Results.Exec_Status;

      use type PGAda.Results.Exec_Status;
   begin
      Result := PGAda.Database.Exec_Prepared
                  (Connection     => Conn,
                   Statement_Name => "updateUsage",
                   Parameters     => To_Unbounded_String (Aliases.To_String (Alias) ) );

      Status := PGAda.Results.Status (Result);

      if Status /= PGAda.Results.Command_OK then
         raise Constraint_Error; -- What to do?
      end if;
   end Touch;

   use type PGAda.Connections.Connection_Status;
begin
   PGAda.Connections.Connect
     (Connection => Conn,
      Info       =>
         "host = "     & FS_Config.Get ("/config/url_shortener/db/host") & " " &
         "dbname = "   & FS_Config.Get ("/config/url_shortener/db/dbname") & " " &
         "user = "     & FS_Config.Get ("/config/url_shortener/db/user") & " " &
         "password = " & FS_Config.Get ("/config/url_shortener/db/password") );

   if PGAda.Connections.Status (Conn) = PGAda.Connections.Connection_Bad then
      raise Constraint_Error with PGAda.Connections.Error_Message (Conn);
   end if;

   PGAda.Database.Prepare
     (Connection     => Conn,
      Statement_Name => "insertAlias",
      Query          => "insert into urls (alias, url) values ($1, $2);",
      Params         => 2);
   PGAda.Database.Prepare
     (Connection     => Conn,
      Statement_Name => "findAlias",
      Query          => "select alias from urls where url = $1;",
      Params         => 1);
   PGAda.Database.Prepare
     (Connection     => Conn,
      Statement_Name => "findURL",
      Query          => "select url from urls where alias = $1;",
      Params         => 1);
   PGAda.Database.Prepare
     (Connection     => Conn,
      Statement_Name => "updateUsage",
      Query          => "update urls set last_accessed = now(), times_accessed = times_accessed + 1 where alias = $1;",
      Params         => 1);
   PGAda.Database.Prepare
     (Connection     => Conn,
      Statement_Name => "findHash",
      Query          => "select alias from urls where alias = $1;",
      Params         => 1);
end URL_Shortener.Database;
