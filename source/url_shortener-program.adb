with AWS.Config.Set;
with AWS.Services.Dispatchers.URI;
with AWS.Server;

with FS_Config;

with URL_Shortener.Frontend;

procedure URL_Shortener.Program is
   Conf        : AWS.Config.Object;
   Server      : AWS.Server.HTTP;
   URI_Handler : AWS.Services.Dispatchers.URI.Handler;
begin -- URL_Shortener.Program
   AWS.Services.Dispatchers.URI.Register
     (Dispatcher => URI_Handler,
      URI        => "/",
      Action     => Frontend.Create_Alias'Unrestricted_Access);

   AWS.Services.Dispatchers.URI.Register_Regexp
     (Dispatcher => URI_Handler,
      URI => "/[0-9A-Za-z]+",
      Action => Frontend.Get_Alias'Unrestricted_Access);

   AWS.Config.Set.Server_Name (O => Conf, Value => "URL Shortener");
   AWS.Config.Set.Server_Port (O => Conf, Value => Positive'Value (FS_Config.Get ("/config/url_shortener/server/port") ) );
   AWS.Config.Set.Server_Host (O => Conf, Value => FS_Config.Get ("/config/url_shortener/server/host") );
   AWS.Config.Set.Case_Sensitive_Parameters (O => Conf, Value => False);
   AWS.Config.Set.Reuse_Address (O => Conf, Value => True);
   AWS.Config.Set.Check_URL_Validity (O => Conf, Value => True);

   AWS.Server.Start (Web_Server => Server, Dispatcher => URI_Handler, Config => Conf);

   AWS.Server.Wait;
end URL_Shortener.Program;
