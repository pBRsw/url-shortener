with Ada.Strings.Unbounded;

with PGAda.Connections;

package URL_Shortener is

   Conn : PGAda.Connections.Connection;

   function To_Unbounded_String (Source : String) return Ada.Strings.Unbounded.Unbounded_String
      renames Ada.Strings.Unbounded.To_Unbounded_String;
   function To_String (Source : Ada.Strings.Unbounded.Unbounded_String) return String renames Ada.Strings.Unbounded.To_String;

end URL_Shortener;
