--
-- PostgreSQL database dump
--

SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

--
-- Name: url_shortener; Type: DATABASE; Schema: -; Owner: url_shortener
--

CREATE DATABASE url_shortener WITH TEMPLATE = template0 ENCODING = 'UTF8';


ALTER DATABASE url_shortener OWNER TO url_shortener;

\connect url_shortener

SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: urls; Type: TABLE; Schema: public; Owner: url_shortener; Tablespace: 
--

CREATE TABLE urls (
    id integer NOT NULL,
    alias character varying(88) NOT NULL,
    url text NOT NULL,
    date_added timestamp without time zone DEFAULT now() NOT NULL,
    times_accessed integer DEFAULT 0 NOT NULL,
    last_accessed timestamp without time zone,
    lifespan interval minute,
    permanent boolean DEFAULT false
);


ALTER TABLE public.urls OWNER TO url_shortener;

--
-- Name: urls_id_seq; Type: SEQUENCE; Schema: public; Owner: url_shortener
--

CREATE SEQUENCE urls_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.urls_id_seq OWNER TO url_shortener;

--
-- Name: urls_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: url_shortener
--

ALTER SEQUENCE urls_id_seq OWNED BY urls.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: url_shortener
--

ALTER TABLE urls ALTER COLUMN id SET DEFAULT nextval('urls_id_seq'::regclass);


--
-- PostgreSQL database dump complete
--

